inherit c make

SRC_URI = "https://downloads.sourceforge.net/project/linuxptp/v${SRC_MAINREV}/linuxptp-${SRC_REV}.tgz"

S = "${SRCDIR}/linuxptp-${SRC_REV}"

COMPATIBLE_HOST_ARCHS = ".*linux"

EXTRA_OEMAKE += "CROSS_COMPILE=${TARGET_PREFIX} prefix=${prefix} mandir=${mandir}"

DEPENDS = "librt libm"

do_install[postfuncs] += "do_install_configs"
do_install_configs() {
    install -D -t ${D}${docdir}/${PN}/configs/ configs/*
}
PACKAGES =+ "${PN}-configs"
FILES_${PN}-configs = "${docdir}/${PN}/configs/*.cfg"

inherit auto-package-utils
AUTO_PACKAGE_UTILS = "phc_ctl pmc hwstamp_ctl timemaster ptp4l phc2sys nsm ts2phc"
AUTO_PACKAGE_UTILS_DEPENDS = "librt libm"
AUTO_PACKAGE_UTILS_RDEPENDS = "librt libm"

RDEPENDS_${PN} = "${AUTO_PACKAGE_UTILS_PROVIDES}"
