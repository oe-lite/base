DESCRIPTION = "Set of i2c tools for linux"

COMPATIBLE_HOST_ARCHS = ".*linux"

inherit autotools auto-package-utils auto-package-libs

require conf/fetch/kernelorg.conf
SRC_URI = "${KERNELORG_MIRROR}/software/utils/i2c-tools/i2c-tools-${PV}.tar.xz"
#SRC_URI += "file://Module.mk"

EXTRA_OEMAKE += "PREFIX=${prefix} EXTRA=eeprog"

AUTO_PACKAGE_LIBS = "i2c"
AUTO_PACKAGE_LIBS_DEPENDS = "libc"
AUTO_PACKAGE_LIBS_RDEPENDS = "libc"

AUTO_PACKAGE_UTILS = "ddcmon decode-dimms decode-edid decode-vaio \
	eeprog i2cdetect i2cdump i2cget i2cset \
	i2c-stub-from-dump i2ctransfer"

AUTO_PACKAGE_UTILS_DEPENDS += "libc libi2c"
AUTO_PACKAGE_UTILS_RDEPENDS += "libc libi2c"

RDEPENDS_${PN}-ddcmon = "perl"
RDEPENDS_${PN}-decode-dimms = "perl"
RDEPENDS_${PN}-decode-edid = "perl"
RDEPENDS_${PN}-decode-vaoi = "perl"

# The i2c-tools package is used to pull in all provided util features
RDEPENDS_${PN} = "${AUTO_PACKAGE_UTILS_PROVIDES}"
