DESCRIPTION = "ISC Internet Domain Name Server"
HOMEPAGE = "http://www.isc.org/sw/bind/"
LICENSE = "ISC"

COMPATIBLE_HOST_ARCHS = ".*linux"

inherit autotools pkgconfig libtool libtool-stage sysvinit

DEPENDS = "libssl libcrypto libcap ${DEPENDS_LIBC}"
DEPENDS_LIBC = "libdl"
DEPENDS_LIBC:HOST_LIBC_mingw = ""
RDEPENDS_${PN} = "${PN}-config"
DEPENDS_${PN} += "${DEPENDS_LIBC} libc libcrypto libssl libcap ${LIBS_AUTO_RPACKAGES}"
RDEPENDS_${PN} += "${DEPENDS_LIBC} libc libcrypto libssl libcap ${LIBS_AUTO_RPACKAGES}"
DEPENDS_${PN}-utils += "${DEPENDS_LIBC} libc libcap libcrypto ${LIBS_AUTO_RPACKAGES}"
RDEPENDS_${PN}-utils += "${DEPENDS_LIBC} libc libcap libcrypto ${LIBS_AUTO_RPACKAGES}"

# These are the files from the ancient conf.patch from openembedded project
SRC_URI += "file://init.d"
SRC_URI += "file://conf/db.0;subdir=conf file://conf/db.127;subdir=conf \
    file://conf/db.255;subdir=conf file://conf/db.empty;subdir=conf \
    file://conf/db.local;subdir=conf file://conf/db.root;subdir=conf \
    file://conf/named.conf;subdir=conf \
    file://conf/named.conf.local;subdir=conf \
    file://conf/named.conf.options;subdir=conf \
    file://conf/zones.rfc1918;subdir=conf"

# --enable-exportlib is necessary for building dhcp
EXTRA_OECONF = "\
    --enable-ipv6=no \
    --with-randomdev=/dev/random \
    --disable-devpoll --disable-epoll \
    --sysconfdir=${sysconfdir}/bind \
    --with-openssl=${TARGET_SYSROOT}${target_prefix} \
    --with-gssapi=no \
    --with-libxml2=no \
    --enable-exportlib \
    --with-export-includedir=${includedir} \
    --with-export-libdir=${libdir} \
    --with-libtool \
"

# 64-bit stdatomic.h functions requires libatomic on powerpc
DEPENDS += "${DEPENDS_STDATOMIC}"
DEPENDS_STDATOMIC = ""
DEPENDS_STDATOMIC:HOST_CPU_powerpc = "libatomic"

RECIPE_FLAGS += "bind_zlib"
EXTRA_OECONF_ZLIB = "--without-zlib"
EXTRA_OECONF_ZLIB:USE_bind_zlib = "--with-zlib"
EXTRA_OECONF += "${EXTRA_OECONF_ZLIB}"
DEPENDS:>USE_bind_zlib = " libz"
DEPENDS_${PN}:>USE_bind_zlib = " libz"
RDEPENDS_${PN}:>USE_bind_zlib = " libz"
DEPENDS_${PN}-utils:>USE_bind_zlib = " libz"
RDEPENDS_${PN}-utils:>USE_bind_zlib = " libz"

RECIPE_FLAGS += "bind_sysvinit_start bind_sysvinit_stop"
SYSVINI_SCRIPT_bind = "bind"
DEFAULT_USE_bind_sysvinit_start= "20"
DEFAULT_USE_bind_sysvinit_stop = "20"

RECIPE_FLAGS += "bind_openssl"
DEFAULT_USE_bind_openssl = True
EXTRA_OECONF += "${EXTRA_OECONF_OPENSSL}"
EXTRA_OECONF_OPENSSL = " --without-openssl"
EXTRA_OECONF_OPENSSL:USE_bind_openssl = ""

# last verified with bind-9.7.2-P3
PARALLEL_MAKE = ""

do_install[postfuncs] =+ "do_install_extra_files"
do_install_extra_files() {
	rm "${D}/usr/bin/nslookup"
	install -d "${D}/etc/bind"
	install -d "${D}/etc/init.d"
	install -m 644 ${SRCDIR}/conf/* "${D}/etc/bind"
	install -m 755 "${SRCDIR}/init.d" "${D}/etc/init.d/bind"
}

inherit auto-package-libs
AUTO_PACKAGE_LIBS_DEV_DEPENDS = "${PN}-dev"

FILES_${PN} += "${libdir}/named/*.so"
FILES_${PN}-dbg += "${libdir}/named/.debug/*.so"

inherit binconfig
BINCONFIG_FILES = "${bindir}/isc-config.sh"
FILES_${PN}-dev += "${bindir}/isc-config.sh"
DEPENDS_${PN}-dev = "libssl libcrypto ${DEPENDS_LIBC}"

# The named configuration file includes named.conf.local,
# hence to make machine/project specific configuration provide
# a named.conf.local and override the "-config" package
FILES_${PN}-config = "${sysconfdir}/bind/named.conf.local"
PACKAGES =+ "${PN}-utils ${PN}-config"
FILES_${PN}-utils = "${bindir}/host ${bindir}/dig ${bindir}/nslookup"
