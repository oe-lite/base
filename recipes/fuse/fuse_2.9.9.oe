DESCRIPTION = "With FUSE it is possible to implement a fully functional filesystem in a userspace program"
HOMEPAGE = "http://fuse.sf.net"
LICENSE = "GPL"

COMPATIBLE_HOST_ARCHS = ".*linux"

inherit autotools-autoreconf pkgconfig

SRC_URI = "https://github.com/libfuse/libfuse/releases/download/fuse-${PV}/fuse-${PV}.tar.gz"
SRC_URI += "file://0002-util-ulockmgr_server-c-conditionally-define-closefrom-fix-glibc-2-34.patch"
SRC_URI += "file://dont-run-updaterc.d-on-host.patch"
SRC_URI += "file://fuse_kernel.h_clean_includes.patch"

DEPENDS = "libpthread librt libdl"

EXTRA_OECONF += " --disable-kernel-module --disable-example"

# Get rid of /dev/fuse (and /dev while at it), which will be created if run
# with mknod privileges
do_install_rm_dev() {
	rm -rf ${D}/dev
}
do_install[postfuncs] += "do_install_rm_dev"

PACKAGES = "${PN}-dbg ${PN} ${PN}-doc"

DEPENDS_${PN} = "libc libpthread"
RDEPENDS_${PN} = "libc libpthread"

inherit auto-package-libs
AUTO_PACKAGE_LIBS = "fuse ulockmgr"
LIBRARY_VERSION_${PN}-libfuse = "2"
LIBRARY_VERSION_${PN}-libulockmgr = "1"
DEPENDS_${PN}-libfuse = "libc libdl libpthread"
RDEPENDS_${PN}-libfuse = "libc libdl libpthread"
DEPENDS_${PN}-libulockmgr = "libc libpthread"
RDEPENDS_${PN}-libulockmgr = "libc libpthread"

FILES_${PN}-libfuse-dev = "${includedir}/fuse*"
FILES_${PN}-libulockmgr-dev = "${includedir}/ulockmgr.h"

FILES_${PN} += "${libdir}/libfuse.so"
FILES_${PN} += "${libdir}/libulockmgr.so"
