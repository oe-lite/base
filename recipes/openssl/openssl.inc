# -*- mode:python; -*-
DESCRIPTION = "Secure Socket Layer (SSL) binary and related \
	cryptographic tools."
HOMEPAGE = "http://www.openssl.org/"
LICENSE = "openssl"

RECIPE_TYPES = "machine native sdk"

#Remove last letter from openssl minor version
FILESPATHPKG = "${PN}-${PV}:${PN}-${@'${PV}'[:-1]}:openssl"

inherit c make pkgconfig

SRC_URI = "https://www.openssl.org/source/openssl-${PV}.tar.gz"

DEPENDS = "${DEPENDS_HOST_OS} libc libgcc libpthread"
DEPENDS_HOST_OS = "libdl"
DEPENDS_HOST_OS:HOST_OS_mingw32 = ""

CFLAG = "${@['-DL_ENDIAN', '-DB_ENDIAN']['${HOST_ENDIAN}'=='b']} ${HOST_CFLAGS}"
CFLAG:>HOST_LIBC_glibc = " -DTERMIO"

CROSS:native = ""
export WINDRES = "${CROSS}windres"
export DIRS = "crypto ssl apps engines"
export EX_LIBS = "-lgcc -ldl"
export AS = "${CC} -c"

do_configure () {
	ln -sf apps/openssl.pod crypto/crypto.pod ssl/ssl.pod doc/

	case "${HOST_OS}" in

	linux*)
		case "${HOST_CPU}" in
		i?86)		target="linux-elf" ;;
		armeb)		target="linux-generic32" ;;
		arm)		target="linux-armv4" ;;
		powerpc64)	target="linux-ppc64" ;;
		powerpc)	target="linux-ppc" ;;
		*)		target="linux-${HOST_CPU}" ;;
		esac
		;;

	mingw*)
		case "${HOST_CPU}" in
		i?86)		target="mingw" ;;
		x86_64)		target="mingw64" ;;
		*)		target="mingw-${HOST_CPU}" ;;
		esac
		;;

	darwin*)
		case "${HOST_CPU}" in
		i?86)		target="darwin-i386-cc" ;;
		x86_64)		target="darwin64-x86_64-cc" ;;
		powerpc)	target="darwin-ppc-cc" ;;
		*)		target="darwin-${HOST_CPU}" ;;
		esac
		;;

	*)
		target="${HOST_OS}-${HOST_CPU}"
		;;
	esac

	# inject machine-specific flags
	sed -i -e "s|^\(\"$target\",\s*\"[^:]\+\):\([^:]\+\)|\1:${CFLAG}|g" Configure

	if [ "${bindir}" != "${prefix}/bin" ] ; then
		echo "Error: bindir (${bindir}) != ${prefix}/bin"
		echo "OpenSSL build systems assumes bin is subdir of prefix"
		exit 1
	fi
	if [ "${libdir}" != "${prefix}/lib" ] ; then
		echo "Error: libdir (${libdir}) != ${prefix}/lib"
		echo "OpenSSL build systems assumes lib is subdir of prefix"
		exit 1
	fi
	if [ "${includedir}" != "${prefix}/include" ] ; then
		echo "Error: includedir (${includedir}) != ${prefix}/include"
		echo "OpenSSL build systems assumes include is subdir of prefix"
		exit 1
	fi

	prefix="${prefix}"
	[ "$prefix" = "" ] && prefix="/"
	# A bug causes prefix to be added for mingw32 builds when
	# using absolute libdir, so make it relative
	libdir="${libdir#${prefix}/}"
	perl ./Configure shared \
		--prefix=$prefix \
		--libdir=$libdir \
		--openssldir=${libdir}/ssl \
		$target
}

do_compile () {
	oe_runmake
}

do_install () {
	oe_runmake -j1 DESTDIR="${D}" MANDIR="${mandir}" install

	# On x86_64, move lib/* to lib64
	if [ "${libdir}" != "${prefix}/lib" ]
	then
		install -d ${D}${libdir} ${D}${libdir}/pkgconfig
		mv ${D}${prefix}/lib/pkgconfig/*.pc ${D}${libdir}/pkgconfig # */
	fi
	install -d ${D}${includedir}
	cp --dereference -R include/openssl ${D}${includedir}
}

DEPENDS_${PN} += " ${PN}-libcrypto ${PN}-libssl libpthread"
RDEPENDS_${PN} += " ${PN}-libcrypto ${PN}-libssl ${PN}-engines ${PN}-misc libpthread"

PACKAGES =+ "${PN}-misc ${PN}-engines"
FILES_${PN}-misc = "${libdir}/ssl/misc ${libdir}/ssl/openssl.cnf"
FILES_${PN}-engines = "${libdir}/engines*/*.so"
FILES_${PN}-engines:HOST_OS_mingw32 = "${libdir}/engines*/*.dll"
FILES_${PN}-engines:HOST_KERNEL_darwin = "${libdir}/engines*/*.dylib"
FILES_${PN}-dbg =+ "${libdir}/engines*/.debug"
FILES_${PN}-doc =+ "${libdir}/ssl/man ${libdir}/ssl/*.cnf*"

inherit auto-package-libs
AUTO_PACKAGE_LIBS = "crypto ssl"
AUTO_PACKAGE_LIBS_PCPREFIX = "lib"
AUTO_PACKAGE_LIBS_DEV_DEPENDS = "${PN}-dev_${PV} libpthread"
AUTO_PACKAGE_LIBS_DEV_RDEPENDS = "${PN}-dev_${PV} libpthread"
inherit auto-package-utils
AUTO_PACKAGE_UTILS = "openssl c_rehash"
DEPENDS_${PN}-openssl += "libc libdl libcrypto libssl libpthread"
RDEPENDS_${PN}-openssl += "libc libdl libcrypto libssl libpthread"
DEPENDS_${PN}-dev = " libpthread"
FILES_${PN}-libcrypto += " ${LIBCRYPTO_FILES}"  
LIBCRYPTO_FILES = ""
LIBCRYPTO_FILES:HOST_OS_mingw32 = "${sharedlibdir}/libeay32.dll"
FILES_${PN}-libssl += " ${LIBSSL_FILES}"
LIBSSL_FILES = ""
LIBSSL_FILES:HOST_OS_mingw32 = "${sharedlibdir}/ssleay32.dll"
DEPENDS_${PN}-libcrypto += " ${DEPENDS} libpthread"
DEPENDS_${PN}-libssl += " ${DEPENDS} libcrypto libpthread"
RDEPENDS_${PN}-libcrypto += "libc libgcc ${DEPENDS_HOST_OS} libpthread"
RDEPENDS_${PN}-libssl += "${DEPENDS_HOST_OS} libcrypto libpthread"
