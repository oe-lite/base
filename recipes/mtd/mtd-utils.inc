# -*- mode:python; -*-
DESCRIPTION = "Tools for managing memory technology devices."
HOMEPAGE = "http://www.linux-mtd.infradead.org/"

RECIPE_TYPES = "machine native sdk"

COMPATIBLE_HOST_ARCHS = ".*linux"

inherit c autotools-autoreconf auto-package-utils pkgconfig

SRC_URI = "git://git.infradead.org/mtd-utils.git;protocol=git;tag=v${PV}"
S = "${SRCDIR}/mtd-utils"

DEPENDS = "libz liblzo2 libuuid libm"

EXTRA_OECONF += "--disable-tests"

EXTRA_OECONF += "--without-xattr"
EXTRA_OECONF += "--without-zstd"
EXTRA_OECONF += "--without-crypto"

AUTO_PACKAGE_UTILS = "\
	bin2nand docfdisk doc_loadbios flashcp flash_erase flash_eraseall \
	flash_info flash_lock flash_otp_dump flash_otp_info flash_unlock \
	ftl_check ftl_format jffs2dump mkbootenv mkfs.jffs2 mkfs.ubifs mkpfi \
	mtd_debug mtdinfo nand2bin nanddump nandtest nandwrite nftldump \
	nftl_format pddcustomize pfi2bin pfiflash recv_image rfddump \
	rfdformat serve_image sumtool ubiattach ubicrc32 \
	ubidetach ubiformat ubigen ubimirror ubimkvol ubinfo ubinize \
	ubirename ubirmvol ubirsvol ubiupdatevol unubi \
"

AUTO_PACKAGE_UTILS_DEPENDS = "libc libgcc"
AUTO_PACKAGE_UTILS_RDEPENDS = "libc libgcc"

FILES_mtd-utils-ubicrc32 = "${sbindir}/ubicrc32.pl"

DEPENDS_${PN}-jffs2reader = "libz liblzo2"
RDEPENDS_${PN}-jffs2reader = "libz liblzo2"
DEPENDS_${PN}-jffs2dump = "libz liblzo2"
RDEPENDS_${PN}-jffs2dump = "libz liblzo2"
DEPENDS_${PN}-mkfs-jffs2 = "libz liblzo2"
RDEPENDS_${PN}-mkfs-jffs2 = "libz liblzo2"
DEPENDS_${PN}-mkfs-ubifs = "libz liblzo2 libuuid libm"
RDEPENDS_${PN}-mkfs-ubifs = "libz liblzo2 libuuid libm"

# The mtd-utils package is used to pull in all provided util features
RDEPENDS_${PN} = "${AUTO_PACKAGE_UTILS_PROVIDES}"
