inherit autotools

SUMMARY = "Text-based modem control and terminal emulation program"

DESCRIPTION = "Minicom is a text-based modem control and terminal emulation program \
for Unix-like operating systems"

HOMEPAGE="https://salsa.debian.org/minicom-team/minicom"

SRC_URI = "https://salsa.debian.org/minicom-team/minicom/-/archive/${PV}/minicom-${PV}.tar.bz2"

COMPATIBLE_HOST_ARCHS = ".*linux"

COMMON_DEPENDS = "libc libiconv libncurses"

DEPENDS += "${COMMON_DEPENDS}"
RDEPENDS += "${COMMON_DEPENDS}"

DEPENDS_${PN} += "${COMMON_DEPENDS}"
RDEPENDS_${PN} += "${COMMON_DEPENDS}"
