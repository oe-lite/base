DESCRIPTION = "A multi-platform support library with a focus on asynchronous I/O"
LICENSE = "MIT"

COMPATIBLE_HOST_ARCHS = ".*linux"
RECIPE_TYPES = "machine"

SRC_URI = "git://github.com/libuv/libuv.git;protocol=https;${SRC_REV}"
S = "${SRCDIR}/${PN}"

DEPENDS += "libpthread"

inherit autotools pkgconfig-install library

do_configure[prefuncs] += "do_configure_autogen"
do_configure_autogen() {
	./autogen.sh
}

LIBRARY_VERSION = "1"
DEPENDS_${PN} += "libc libpthread"
