SUMMARY = "Yet Another JSON Library."
DESCRIPTION = "YAJL is a small event-driven (SAX-style) JSON parser \
written in ANSI C, and a small validating JSON generator."
HOMEPAGE = "http://lloyd.github.com/yajl/"

COMPATIBLE_HOST_ARCHS = ".*linux"

inherit cmake library

LICENSE = "ISC"

SRC_URI = "git://github.com/lloyd/yajl;protocol=https;${SRC_REV}"
SRC_REV = "tag=${PV}"
S = "${SRCDIR}/yajl"

DEPENDS += "libm libc"
DEPENDS_${PN} += "libm libc"
RDEPENDS_${PN} += "libm libc"

FILES_${PN} += "${bindir}"

LIBRARY_VERSION = "2"

inherit auto-package-utils
AUTO_PACKAGE_UTILS = "json_reformat json_verify"
AUTO_PACKAGE_UTILS_DEPENDS = "libc"
AUTO_PACKAGE_UTILS_RDEPENDS = "libc"
DEPENDS_${PN}-json-reformat += "libm"
RDEPENDS_${PN}-json-reformat += "libm"
