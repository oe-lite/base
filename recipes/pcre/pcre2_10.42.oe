DESCRIPTION = "A set of C functions that implement regular expression pattern \
	matching use the same syntax and semantics as Perl 5. PCRE2 was first \
	released in 2015 to replace the API in the original PCRE library, \
	which is now obsolete and no longer maintained."
HOMEPAGE = "https://github.com/PCRE2Project/pcre2"
LICENSE = "BSD-3-Clause"

RECIPE_TYPES = "native machine sdk"

SRC_URI = "https://github.com/PCRE2Project/pcre2/releases/download/pcre2-10.42/pcre2-${PV}.tar.bz2"

inherit autotools pkgconfig binconfig

inherit auto-package-libs
AUTO_PACKAGE_LIBS = "pcre2-8 pcre2-posix"
LIBRARY_VERSION_${PN}-libpcre2-8 = "0"
LIBRARY_VERSION_${PN}-libpcre2-posix = "3"
AUTO_PACKAGE_LIBS_PCPREFIX = "lib"
FILES_${PN}-libpcre2-8 += "${includedir}/pcre2.h"
FILES_${PN}-libpcre2-posix += "${includedir}/pcre2posix.h"
AUTO_PACKAGE_LIBS_DEPENDS = "libc"
AUTO_PACKAGE_LIBS_RDEPENDS = "libc"
AUTO_PACKAGE_LIBS_DEV_DEPENDS = "${PN}-dev"
DEPENDS_${PN}-libpcre2-posix += "libpcre2-8"
RDEPENDS_${PN}-libpcre2-posix += "libpcre2-8"

inherit auto-package-utils
AUTO_PACKAGE_UTILS = "pcre2grep pcre2test"
AUTO_PACKAGE_UTILS_DEPENDS = "libpcre2-8"
AUTO_PACKAGE_UTILS_RDEPENDS = "libpcre2-8"
DEPENDS_${PN}-pcre2test += "libpcre2-posix"
RDEPENDS_${PN}-pcre2test += "libpcre2-posix"
