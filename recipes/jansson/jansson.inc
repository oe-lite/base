DESCRIPTION = "Jansson is a C library for encoding, decoding and manipulating \
JSON data"
HOMEPAGE = "www.digip.org/jansson/"
LICENSE = "MIT"
RECIPE_TYPES = "machine native sdk"

SRC_URI = "http://www.digip.org/jansson/releases/jansson-${PV}.tar.bz2"

inherit autotools
AUTOTOOLS_DISTCLEAN = "0"

RECIPE_FLAGS += "jansson_urandom"
DEFAULT_USE_jansson_urandom = True
EXTRA_OECONF += "${EXTRA_OECONF_URANDOM}"
EXTRA_OECONF_URANDOM = "--disable-urandom"
EXTRA_OECONF_URANDOM:USE_jansson_urandom = "--enable-urandom"

inherit library

DEPENDS_${PN} += "libc"
